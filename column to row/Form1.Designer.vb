﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.InputBox = New System.Windows.Forms.TextBox()
        Me.ClearListButton = New System.Windows.Forms.Button()
        Me.RunButton = New System.Windows.Forms.Button()
        Me.sProgress = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TextDelim = New System.Windows.Forms.TextBox()
        Me.TrimButton = New System.Windows.Forms.Button()
        Me.ShortArrayButton = New System.Windows.Forms.Button()
        Me.UpperCaseButton = New System.Windows.Forms.Button()
        Me.LowerCaseButton = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'InputBox
        '
        Me.InputBox.AcceptsReturn = True
        resources.ApplyResources(Me.InputBox, "InputBox")
        Me.InputBox.Name = "InputBox"
        Me.InputBox.TabStop = False
        '
        'ClearListButton
        '
        resources.ApplyResources(Me.ClearListButton, "ClearListButton")
        Me.ClearListButton.Name = "ClearListButton"
        Me.ClearListButton.TabStop = False
        Me.ClearListButton.UseVisualStyleBackColor = True
        '
        'RunButton
        '
        resources.ApplyResources(Me.RunButton, "RunButton")
        Me.RunButton.Name = "RunButton"
        Me.RunButton.TabStop = False
        Me.RunButton.UseVisualStyleBackColor = True
        '
        'sProgress
        '
        resources.ApplyResources(Me.sProgress, "sProgress")
        Me.sProgress.Name = "sProgress"
        '
        'Label14
        '
        resources.ApplyResources(Me.Label14, "Label14")
        Me.Label14.Name = "Label14"
        '
        'Label1
        '
        resources.ApplyResources(Me.Label1, "Label1")
        Me.Label1.Name = "Label1"
        '
        'TextDelim
        '
        resources.ApplyResources(Me.TextDelim, "TextDelim")
        Me.TextDelim.Name = "TextDelim"
        '
        'TrimButton
        '
        resources.ApplyResources(Me.TrimButton, "TrimButton")
        Me.TrimButton.Name = "TrimButton"
        Me.TrimButton.TabStop = False
        Me.TrimButton.UseVisualStyleBackColor = True
        '
        'ShortArrayButton
        '
        resources.ApplyResources(Me.ShortArrayButton, "ShortArrayButton")
        Me.ShortArrayButton.Name = "ShortArrayButton"
        Me.ShortArrayButton.TabStop = False
        Me.ShortArrayButton.UseVisualStyleBackColor = True
        '
        'UpperCaseButton
        '
        resources.ApplyResources(Me.UpperCaseButton, "UpperCaseButton")
        Me.UpperCaseButton.Name = "UpperCaseButton"
        Me.UpperCaseButton.TabStop = False
        Me.UpperCaseButton.UseVisualStyleBackColor = True
        '
        'LowerCaseButton
        '
        resources.ApplyResources(Me.LowerCaseButton, "LowerCaseButton")
        Me.LowerCaseButton.Name = "LowerCaseButton"
        Me.LowerCaseButton.TabStop = False
        Me.LowerCaseButton.UseVisualStyleBackColor = True
        '
        'Form1
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.LowerCaseButton)
        Me.Controls.Add(Me.UpperCaseButton)
        Me.Controls.Add(Me.ShortArrayButton)
        Me.Controls.Add(Me.TrimButton)
        Me.Controls.Add(Me.TextDelim)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.sProgress)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.RunButton)
        Me.Controls.Add(Me.ClearListButton)
        Me.Controls.Add(Me.InputBox)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents InputBox As System.Windows.Forms.TextBox
    Friend WithEvents ClearListButton As System.Windows.Forms.Button
    Friend WithEvents RunButton As System.Windows.Forms.Button
    Friend WithEvents sProgress As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TextDelim As System.Windows.Forms.TextBox
    Friend WithEvents TrimButton As System.Windows.Forms.Button
    Friend WithEvents ShortArrayButton As System.Windows.Forms.Button
    Friend WithEvents UpperCaseButton As System.Windows.Forms.Button
    Friend WithEvents LowerCaseButton As System.Windows.Forms.Button

End Class
