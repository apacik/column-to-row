﻿Public Class Form1

    Dim sText, strComputer As String
    Dim sArray() As String
    Dim s As Integer

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'Initialize before program startup
        TextDelim.Text = ", "
        InputBox.Select()

    End Sub

    Private Sub ClearListButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ClearListButton.Click

        InputBox.Clear()
        sProgress.Text = 0
        TextDelim.Text = ", "
        InputBox.Select()

    End Sub

    Private Sub InputBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles InputBox.TextChanged

        sText = InputBox.Text

        'replace different new line characters with one version and remove spaces
        sText = sText.Replace(vbCrLf, vbCr)
        sText = sText.Replace(vbLf, vbCr)
        sText = sText.Replace(vbTab, vbNullString)

        'remove last carriage return if it exists
        If sText.EndsWith(vbCr) Then
            sText = sText.Substring(0, sText.Length - 1)
        End If

        'split each line in to an array
        sArray = sText.Split(vbCr)

        sProgress.Text = UBound(sArray) + 1

    End Sub

    Private Sub Inputbox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles InputBox.KeyPress

        If e.KeyChar = Convert.ToChar(1) Then
            DirectCast(sender, TextBox).SelectAll()
            e.Handled = True
        End If

    End Sub

    Private Sub GenerateQry_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RunButton.Click

        Dim sText As String
        Dim Delim As String

        If InputBox.Text = "" Then
            MessageBox.Show("List is empty !", "", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If

        sText = ""
        For s = 0 To UBound(sArray)
            sArray(s) = sArray(s)
        Next s

        'sText = Join(sArray, ", ")
        Delim = TextDelim.Text
        sText = Join(sArray, Delim)

        Clipboard.Clear()
        Clipboard.SetDataObject(sText)

        Cursor = Cursors.AppStarting
        System.Threading.Thread.Sleep(500)
        Cursor = Cursors.Default

        Dim MyAppID
        MyAppID = Shell("NOTEPAD.EXE", 1)
        AppActivate(MyAppID)
        System.Windows.Forms.SendKeys.Send("^V") ' send CTRL-V

    End Sub

    Private Sub TrimButton_Click(sender As System.Object, e As System.EventArgs) Handles TrimButton.Click

        If InputBox.Text = Nothing Then
            MessageBox.Show("List is empty !", "", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Exit Sub
        End If

        sText = ""
        For s = 0 To UBound(sArray)
            sText = sText & sArray(s) & vbCrLf
            sText = sText.Replace(" ", vbNullString)
        Next s

        'write sorted text
        InputBox.Text = sText

        'remove text selection
        InputBox.Select(0, 0)

    End Sub

    Private Sub ShortArray_Click(sender As System.Object, e As System.EventArgs) Handles ShortArrayButton.Click

        If InputBox.Text = Nothing Then
            MessageBox.Show("List is empty !", "", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Exit Sub
        End If

        Array.Sort(sArray)

        sText = ""
        For s = 0 To UBound(sArray)
            sText = sText & sArray(s) & vbCrLf
        Next s

        'write sorted text
        InputBox.Text = sText

        'remove text selection
        InputBox.Select(0, 0)

    End Sub

    Private Sub UpperCaseButton_Click(sender As System.Object, e As System.EventArgs) Handles UpperCaseButton.Click

        If InputBox.Text = Nothing Then
            MessageBox.Show("List is empty !", "", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Exit Sub
        End If

        sText = ""
        For s = 0 To UBound(sArray)
            sText = sText & sArray(s).ToUpper & vbCrLf
        Next s

        'write sorted text
        InputBox.Text = sText

        'remove text selection
        InputBox.Select(0, 0)

    End Sub

    Private Sub LowerCaseButton_Click(sender As System.Object, e As System.EventArgs) Handles LowerCaseButton.Click

        If InputBox.Text = Nothing Then
            MessageBox.Show("List is empty !", "", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Exit Sub
        End If

        sText = ""
        For s = 0 To UBound(sArray)
            sText = sText & sArray(s).ToLower & vbCrLf
        Next s

        'write sorted text
        InputBox.Text = sText

        'remove text selection
        InputBox.Select(0, 0)

    End Sub
End Class

