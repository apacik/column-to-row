Tool to convert text in lines into one row with user defined delimiter.
Also allow to sort text alphabetically, set all text to upper or lower case and trim spaces before and after text in every line.
Result is saved into clipboard and pasted into notepad.


![column_to_row.PNG](https://bitbucket.org/repo/o4xKGq/images/3303605357-column_to_row.PNG)